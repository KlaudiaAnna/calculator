using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                InputConverter inputConverter = new InputConverter();
                CalculatorEngine calculatorEngine = new CalculatorEngine();

                double firtN = inputConverter.ConvertertInputToNumberic(Console.ReadLine());
                double secondN = inputConverter.ConvertertInputToNumberic(Console.ReadLine());
                string operation = Console.ReadLine();


                double result = calculatorEngine.Calculate(operation, firtN, secondN);

                Console.WriteLine(result);
            }
            catch (Exception ex)
            {
                //In real world we would want to log this message
                Console.WriteLine(ex.Message);


            }
        }
    }
}
