﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    public class CalculatorEngine
    {
        public double Calculate(string argOperation, double argFirtsNumber, double argSecondNumber)
        {
            double result;
            switch (argOperation.ToLower())
            {
                case "add":
                case "+":
                    result = argFirtsNumber + argSecondNumber;
                    break;

                case "subtract":
                case "-":
                    result = argFirtsNumber - argSecondNumber;
                    break;
                case "multiply":
                case "*":
                    result = argFirtsNumber * argSecondNumber;
                    break;
                case "divide":
                case "/":
                    result = argFirtsNumber * argSecondNumber;
                    break;
                default:
                    throw new InvalidOperationException("Spcified operation is not recognized");
            }
            return result;
        }
    }

}